# Git workflow summary 

## Introduction

This document describes the general worflow that the Data Science team
plans to use.

It also includes a reminder of the Git commands that are usually needed in each
step, but these are by no means restrictive.

This process assumes that we're working on one enviroment and we all have
access to a GitLab server.

Any recommendations here assume the use of the command line as the tool to
manage Git. If your choice is different make sure you can do what is needed and
that the tool doesn't limit your capabilities.


## Overview

### High level dynamics of the workflow

  1. Alice starts a project locally.
  2. Alice creates a clone of that project in a remote location.
  3. Bob joins the project.
  4. Bob clones the remote version of the project to his local environment.
  5. Bob works locally on a feature on top of Alice's work, without tempering
  with it.
  6. Bob sends it to the remote location and asks for a revision by someone
  else (probably Alice? and maybe others)
  7. Alice (+ others) give a thumbs up/down to Bob's work.  
  8. (a) If all thumbs are up, Bob locally merges his work with the latest version in the
  remote location and makes that change available to everyone.  
  (b). If any thumb is down, Bob discusses with the person that wasn't happy
  with his work and either convinces her or fixes the concerns.

### Specific workflow

  1. Preparation: configure the Git user
  2. Preparation: configure GitLab user
  3. Preparation: configure ssh key (optional)
  4. Start project: Initialise Git repo locally
  5. Start project: Add `.gitignore` and `README.md`
  6. Start project: Decide whether a _development_ branch is going to be used
  7. Share the project: Create a project on the remote Git server (GitLab)
  8. Share the project: Give permissions to the team members
  9. Share the project: Push all relevant branches of the the local project to
  the remote location
  10. Share the project: Notify the team members
  11. Work on feature: Clone the repository locally
  12. Work on feature: Create new branch to work on the featureX
  13. Work on feature: Work on featureX
  14. Work on feature: Clean up commit history
  15. Work on feature: Share branch
  16. Merge feature: Ask for a review of featureX from 1 or 2 team members 
  17. Merge feature: Make chages until all reviewers give a thumbs up
  18. Merge feature: Merge featureX locally (non fast forward)
  19. Merge feature: Make the merged feature public
  20. Merge feature: Remove merged branch


## Detailed steps and required commands (cheatsheet)

### A. Preparation (first time only configuration)

  - Username configured

  ```
  $ git config --global user.name "John Doe"
  $ git config --global user.email johndoe@example.com
  ```

  - GitLab user configured & ssh key configured (optional)

  Follow GitLab's instructions.

 
### B. Start project (4, 5, 6) 

  - Create a new local directory with a meaningful name 
  - Initialise the repository
  ```
  $ cd new_repo
  $ git init
  ```

  - Add a `.gitignore`
  ```
  $ touch .gitignore
  $ git add .gitignore
  $ git commit -m 'chore: add gitignore'
  ```

  - Write a minimal `README.md` that explains the objectives and boundaries of the
  project.
  ```
  $ vim README.md
  $ git add README.md
  $ git commit -m 'doc: add readme'
  ```

  - Add a _development_ branch (optional)
  - Create a new branch (with a meaninguful name)
  ```
  $ git checkout -b feature1
  ```
  
  And start working on _feature1_
  

### C. Share the project (7, 8, 9, 10) 

  - Go to the Git server - e.g. GitLab
  - Create a new repository within the GitLab Group you want 
  - Add the newly created repository as one of the local repository's remotes:
  ```
  $ git remote add <address>.git
  ```

  - Clone your local repository to the remote repository (by default called
    origin)
  ```
  $ git push -u origin master
  ```

  - If you have other branches, push them separately e.g.
  ```
  $ git push origin development
  ```

  - or
  ```
  $ git push origin feature1
  ```

  - Tell the team the project is available   
  - On GitLab ensure that the components of the team have the right permissions
    to work on the project


### D. Work on project (11, 12, 13) 

  - Clone the repository (*repository_name*) from the upstream repo
  ```
  $ git clone <remote_repository_address>
  ```

  - Create a new branch to work on _feature2_
  ```
  $ cd <repository name>
  $ git checkout -b feature2
  ```


### E. Merge feature: Make work public and ask for a revision (14, 15, 16)
  
  - Make sure your commit history makes sense (rewrite history)
  ```
  $ git rebase -i HEAD~<number of commits you want to review>
  ``` 

  - Make sure your _feature2_ branch is based on the latest _master_ (or
    _development_ if that's available)
  ```
  $ git checkout master  # or development
  $ git pull --rebase=preserve origin master
  $ git checkout feature2
  $ git rebase master
  ```

  - Share the _feature2_ branch
  ```
  $ git push origin _feature2_
  ```

  - If _feature2_ is ready, from GitLab send a merge request 

Note that we have assumed that no one tempered with the same branch in
    between, if that was not the case we would have needed a `rebase` of
    _feature2_ after the local `$ git rebase -i (...)`
 
  
### F. Merge feature: Review someone else's branch (17)
  
  - When someone asks you to review their _feature1_, if you need to look at it
    locally, get it from the upstream
    repository
    ```
    $ git fetch feature1 
    $ git checkout -b feature1 origin/feature1 
    ```

  - Check it and send an answer to the person that requested the review:
    + either a thumbs up, or
    + a thumbs down and a reason why you think a review is needed

### G. Merge feature: merge & clean up (18, 19, 20)

  - Merge locally
  ```
  $ git checkout master
  $ git merge --no-ff feature2
  ```
  
  - Delete the working branch on your local repository  
  ```
  $ git branch -d feature2
  ```

  - Make sure you have the latest version of master
  ``` 
  $ git pull --rebase=preserve origin master
  ```

  - Share the new master
  ```
  $ git push origin master
  ```

  - Delete the remote _feature2_
  ```
  $ git push origin :feature2
  ```

  - Let everybody know you have updated upstream master
  
  - Everyone else get the changes
  ```
  $ git checkout master
  $ git fetch --all
  $ git remote prune origin
  ```



## References

https://git-scm.com/  
http://nvie.com/posts/a-successful-git-branching-model/  
http://www.kdgregory.com/index.php?page=scm.git  
https://about.gitlab.com/2014/09/29/gitlab-flow/  
